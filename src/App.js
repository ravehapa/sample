import React, { useState, useEffect } from "react";
import './App.css';

import { OperatingHours, MinutesSelection } from "./utils/constants";

function App() {
	const [products, setProducts] = useState([]);
	const [orders, setOrders] = useState([]);
	const [orderSuccess, setOrderSuccess] = useState(true);

	useEffect(() => {
		setProducts([
			{
				id: 1,
				name: "Americano",
				price: 75.00,
				image: "https://images.squarespace-cdn.com/content/v1/5a7cbe247131a5f17b3cc8fc/1519447742018-MOHBW2G0VOQ7QSCPJE14/Americano-Coffee-Lounge-Ingredients.jpg?format=500w"
			},
			{
				id: 2,
				name: "Latte",
				price: 85.00,
				image: "https://www.allrecipes.com/thmb/W-YsZHeTOds0Pq_mnqSXP33xchg=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/9428203-9d140a4ed1424824a7ddd358e6161473.jpg"
			},
			{
				id: 3,
				name: "Black Coffee",
				price: 65.00,
				image: "https://images.pexels.com/photos/1252195/pexels-photo-1252195.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
			},
		]);
	}, []);

	if(orderSuccess) {
		return <div style={{ height: "100vh", width: "100%", display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column" }}>
			<h2>Order Successful!</h2>
			<div>
				<button 
					type="button"
					style={{ backgroundColor: "white", border: "0px" }}
					onClick={() => {
						setOrderSuccess(false);
					}}
				>
					<h4 style={{ color: "blue" }}>Order for more!</h4>
				</button>
			</div>
			
		</div>
	}

	return (
		<div>
			<div style={{ backgroundColor: "#a9c27f" }}>
				<img 
					src={require("./assets/images/UPWhite.png")}
					style={{ 
						height: "10vh", 
						width: "auto" ,
					}} 
				/>
			</div>

			<h1 style={{ height: "5%", marginTop: 20, marginLeft: 20 }}>Choose your order:</h1>

			<div className='row'>
				<div className='col-lg-9 col-sm-12'>
					{products.map((indiv_product, product_key) => {
						return <div 
							key={product_key} 
							style={{ 
								padding: "5%",
							}} 
							className="col-lg-4 col-sm-12 col-md-6"
						>
							<div 
								style={{
									width: "100%", 
									height: "100%",
									borderRadius: 5,
									overflow: "hidden",
									border: "1px solid gainsboro",
									boxShadow: "4px 4px 10px lightgrey"
								}}
							>
								<div>
									<img 
										src={indiv_product.image}
										style={{ width: "100%", height: 250, objectFit: "cover" }}
									/>
								</div>
								<div 
									style={{ 
										padding: 10, 
										borderBottomWidth: 1, 
										borderBottomColor: "black",
										display: "flex",
										justifyContent: "space-between"
									}}
								>
									<h3 style={{ fontWeight: "bold" }}>{indiv_product.name.toUpperCase()}</h3>
									<h3 style={{ fontWeight: "bold" }}>Php. {indiv_product.price.toFixed(2)}</h3>
								</div>
								
								<div style={{ display: "flex", justifyContent: "center", marginBottom: 10, marginTop: 10 }}>
									<button 
										type="button"
										style={{ 
											backgroundColor: "white", 
											border: "1px solid lightgrey", 
											padding: "10px 30px" 
										}}
										onClick={() => {
											let newOrders = [...orders];

											let order_exist = false;

											let total_qty = 0;

											newOrders.forEach((indiv_order) => {
												total_qty += indiv_order.qty;

												if(indiv_order.id === indiv_product.id && total_qty < 5) {
													indiv_order.qty += 1;
													order_exist = true;
												}
											});

											if(!order_exist) {
												newOrders.push({ ...indiv_product, qty: 1 });
											}

											if(total_qty < 5) {
												setOrders(newOrders);
											}

										}}
									>
										Add to Order
									</button>
								</div>
							</div>
						</div>
					})}
				</div>
				<div className='col-lg-3 col-sm-12 col-md-12'>
					<OrderComponent orders={orders} setOrders={setOrders} setOrderSuccess={setOrderSuccess} />
				</div>
			</div>

			<div style={{ backgroundColor: "#a9c27f", display: "flex", justifyContent: "center", alignItems: "center", height: "3vh", bottom: 0, position: "fixed", width: "100%" }}>
				<small style={{ textAlign: "center" }}>Made with love by Raven, all rights reserve 2022.</small>
			</div>
		</div>
	);
}

export const OrderComponent = ({ orders, setOrders, setOrderSuccess }) => {
	const [selectedPickupType, setSelectedPickupType] = useState("asap");
	const [selectedPickupHour, setSelectedPickupHour] = useState(8);
	const [selectedPickupMinute, setSelectedPickupMinute] = useState(0);
	const [isLoading, setIsLoading] = useState(false);

	let total_price = 0;
	let total_qty = 0;

	const submitHandler = async () => {
		setIsLoading(true);
		// let newOrders = [];

		// orders.forEach(indiv_order => {
		// 	newOrders.push({ id: indiv_order.id, qty: indiv_order.qty });
		// });

		// let postBody = {
		// 	selectedPickupType,
		// 	selectedPickupHour,
		// 	selectedPickupMinute,
		// 	orders: newOrders
		// }

		// try {
		// 	const { data, status } = await axios.post(API_URL + "/submit_orders", postBody);
			
		// 	if(status === 200) {
		// 		setOrders([]);
		//		setSelectedPickupHour(8);
		//		setSelectedPickupMinute(0);
		//		setSelectedPickupType("today");
		// 	}
		// } catch(err) {
		// 	console.log(err);
		// }
		setTimeout(() => {
			setIsLoading(false);
			setOrders([]);
			setSelectedPickupHour(8);
			setSelectedPickupMinute(0);
			setSelectedPickupType("today");
			setOrderSuccess(true);
		}, 2000);

	}

	return <div style={{ marginBottom: "5vh", padding: 10 }}>
		<h3>Your order/s:</h3>
		<div  style={{ display: "flex", justifyContent: "space-between", padding: 20 }}>
			<h4 style={{ width: "60%" }}>Item</h4>
			<h4 style={{ width: "20%", textAlign: "center" }}>Qty</h4>
			<h4 style={{ width: "30%", textAlign: "right" }}>Price</h4>
		</div>
		{orders.length ? orders.map((indiv_order, order_index) => {
			total_price += indiv_order.qty * indiv_order.price;
			total_qty += indiv_order.qty;

			return <div key={order_index}>
				<div style={{ display: "flex", justifyContent: "space-between", padding: 20 }}>
					<p style={{ width: "60%" }}>
						{indiv_order.name}
						<button
							type="button"
							style={{ backgroundColor: "white", border: "0px", color: "red" }}
							onClick={() => {
								let newOrders = [...orders];
								newOrders.splice(order_index, 1);
								setOrders(newOrders);
							}}
						>
							(delete)
						</button>
					</p>
					<p style={{ width: "20%", textAlign: "center" }}>{indiv_order.qty} pc{indiv_order.qty > 1 && "s"}</p>
					<p style={{ width: "30%", textAlign: "right" }}>{(indiv_order.qty * indiv_order.price).toFixed(2)}</p>
				</div>
			</div>
		}) : <p style={{ textAlign: "center" }}>No order</p>}

		{Boolean(total_price) && <div style={{ borderTop: "1px solid black", padding: 20, display: "flex", justifyContent: "space-between" }}>
			<h4>Total</h4>
			<h4>Php. {total_price.toFixed(2)}</h4>
		</div>}

		{Boolean(total_qty >= 5) && <div style={{ marginBottom: "5%" }}>
			<h4 style={{ textAlign: "center", color: "indianred" }}>Max order of 5pcs only!</h4>
		</div>}

		{Boolean(total_price) && <div>
			<h3 style={{ marginBottom: 10 }}>When will you pick-up?</h3>
			<div style={{ marginBottom: 10 }}>
				<span style={{ fontWeight: "bold" }}>Pick-up type: </span>
				<select
					name="pickup_type"
					value={selectedPickupType}
					onChange={(e) => {
						setSelectedPickupType(e.target.value);
					}}
				>
					<option value="asap">ASAP</option>
					<option value="today">Today</option>
					<option value="tomorrow">Tomorrow</option>
				</select>
			</div>

			<div style={{ marginBottom: 10 }}>
				<span style={{ fontWeight: "bold" }}>Pick-up time: </span>
				<select 
					name="pickup_hours"
					value={selectedPickupHour}
					onChange={(e) => {
						setSelectedPickupHour(e.target.value);
					}}
				>
					{OperatingHours.map((indiv_operating_hour, operating_hour_index) => {
						return <option 
							key={operating_hour_index} 
							value={indiv_operating_hour}
						>
							{indiv_operating_hour}
						</option>
					})}
				</select>

				<span style={{ fontWeight: "bold" }}> : </span>
				<select 
					name="pickup_hours"
					value={selectedPickupMinute}
					onChange={(e) => {
						setSelectedPickupMinute(e.target.value);
					}}
				>
					{MinutesSelection.map((indiv_minutes_selection, minutes_selection_index) => {
						return <option 
							key={minutes_selection_index} 
							value={indiv_minutes_selection}
						>
							{indiv_minutes_selection}{!Boolean(indiv_minutes_selection) && 0}
						</option>
					})}
				</select>

				<span> {selectedPickupHour > 7 && selectedPickupHour < 12 ? "AM" : "PM"}</span>
			</div>
			
			<div style={{ display: "flex", justifyContent: "center", marginTop: "5%" }}>
				<button
					type="button"
					style={{ 
						backgroundColor: isLoading ? "gainsboro" : "#fff2cd", 
						border: "0px", 
						margin: "0px auto",
						width: "75%", 
						padding: 10, 
						borderRadius: 5,
					}}
					onClick={submitHandler}
					disabled={isLoading}
				>
					Submit Order
				</button>
			</div>
			
		</div>}
	</div>
}

export default App;
